package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

public class Login {

	private JFrame frame;
	private JTextField textFieldUsuario;
	private JPasswordField passwordFieldUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panelBienvenidaN = new JPanel();
		frame.getContentPane().add(panelBienvenidaN, BorderLayout.NORTH);
		panelBienvenidaN.setLayout(new BoxLayout(panelBienvenidaN, BoxLayout.Y_AXIS));
		
		JLabel lblNewLabel = new JLabel("Bien venido a AppChat");
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel.setFont(new Font("SimHei", Font.PLAIN, 38));
		panelBienvenidaN.add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.BLACK);
		panelBienvenidaN.add(separator);
		
		JPanel panelBotoneraS = new JPanel();
		frame.getContentPane().add(panelBotoneraS, BorderLayout.SOUTH);
		panelBotoneraS.setLayout(new BorderLayout(0, 0));
		
		JPanel panelBotonesIzq = new JPanel();
		panelBotoneraS.add(panelBotonesIzq, BorderLayout.WEST);
		
		JButton buttonAcep = new JButton("Aceptar");
		buttonAcep.setHorizontalAlignment(SwingConstants.LEFT);
		buttonAcep.setFont(new Font("SimHei", Font.PLAIN, 11));
		panelBotonesIzq.add(buttonAcep);
		
		JButton buttonReg = new JButton("Registrarse");
		buttonReg.setFont(new Font("SimHei", Font.PLAIN, 11));
		panelBotonesIzq.add(buttonReg);
		
		JPanel panelBotoneraD = new JPanel();
		panelBotoneraS.add(panelBotoneraD, BorderLayout.EAST);
		
		JButton buttonSalir = new JButton("Salir");
		buttonSalir.setFont(new Font("SimHei", Font.PLAIN, 11));
		panelBotoneraD.add(buttonSalir);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(Color.BLACK);
		panelBotoneraS.add(separator_1, BorderLayout.NORTH);
		
		JPanel panelUserPass = new JPanel();
		frame.getContentPane().add(panelUserPass, BorderLayout.CENTER);
		panelUserPass.setLayout(new BoxLayout(panelUserPass, BoxLayout.Y_AXIS));
		
		JPanel panelUser = new JPanel();
		panelUserPass.add(panelUser);
		panelUser.setLayout(new BorderLayout(0, 0));
		
		JPanel panelUserCent = new JPanel();
		panelUser.add(panelUserCent, BorderLayout.SOUTH);
		
		JLabel lblUsuario = new JLabel("Usuario: ");
		lblUsuario.setFont(new Font("SimHei", Font.BOLD, 14));
		panelUserCent.add(lblUsuario);
		
		textFieldUsuario = new JTextField();
		textFieldUsuario.setFont(new Font("SimHei", Font.PLAIN, 14));
		panelUserCent.add(textFieldUsuario);
		textFieldUsuario.setColumns(10);
		
		JPanel panelPass = new JPanel();
		panelUserPass.add(panelPass);
		panelPass.setLayout(new BorderLayout(0, 0));
		
		JPanel panelPassCent = new JPanel();
		panelPass.add(panelPassCent, BorderLayout.NORTH);
		
		JLabel lblPass = new JLabel("Contrasenya:");
		lblPass.setFont(new Font("SimHei", Font.BOLD, 14));
		panelPassCent.add(lblPass);
		
		passwordFieldUsuario = new JPasswordField();
		passwordFieldUsuario.setFont(new Font("SimHei", Font.PLAIN, 14));
		passwordFieldUsuario.setColumns(10);
		panelPassCent.add(passwordFieldUsuario);
	}

}
